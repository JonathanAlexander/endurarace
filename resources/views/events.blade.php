<x-app-layout>
    <x-slot name="header">
        <div style="float: right">

            <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                <x-jet-nav-link href="{{ route('event/create') }}" :active="request()->routeIs('event/create')">
                           {{ __('create an event') }}
                </x-jet-nav-link>
            </div>

        </div>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Events') }}
        </h2>

    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">

                    <div class="mt-2 text-xl">
                     Events
                    </div>

                    <div class="mt-2 text-gray-500">
                    EnduraRace LLC is a company committed to bringing the best in ultra marathon and adventure events to cyclists and runners alike.
                    </div>
                </div>
            </div>


        </div>
    </div>
</x-app-layout>
